function filter(elements, cb) {
    filtered = []
    for (element of elements) {
        if (cb(element) == true) {
            filtered.push(element)
        }
    }
    return filtered
}

module.exports = filter