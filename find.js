function find(elements, cb) {
    for (element of elements) {
        if (cb(element) == true){
            return element
        }
    }
}

module.exports = find