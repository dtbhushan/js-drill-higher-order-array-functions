const flatten = require("../flatten")

nestedArray = [1, [2], [[3]], [[[4]]]];

flatArray = flatten(nestedArray)
console.log(flatArray)